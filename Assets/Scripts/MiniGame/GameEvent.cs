﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvent : MonoBehaviour
{
    public static GameEvent instance;
    public event Action onDoorTrigger_Enter;
    public event Action onDoorTrigger_Exit;

    private void Awake()
    {
        instance = instance==null ? this : this;
    }


    public void OnDoorTriggerEnter()
    {
        if(onDoorTrigger_Enter != null)
        {
            Debug.Log("INVOKE OnDoorTriggerEnter");
            onDoorTrigger_Enter?.Invoke();
        }
    }

    public void OnDoorTriggerExit()
    {
        if (onDoorTrigger_Exit != null)
        {
            Debug.Log("INVOKE OnDoorTriggerExit");
            onDoorTrigger_Exit?.Invoke();
        }
    }
}

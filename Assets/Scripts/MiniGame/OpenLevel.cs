﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UIElements;

public class OpenLevel : MonoBehaviour
{
    public bool is_open = false;
    private Vector3 init_Pos;

    public Transform oldLevel;
    public Transform newLevel;
    public Material open_trigger;
    public Material close_trigger;
    

    private void Start()
    {
        init_Pos = newLevel.transform.position;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            TriggerNewLevel();
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (is_open)
            {
                StartCoroutine(MoveObj.MoveAtoB(oldLevel, new Vector3(oldLevel.position.x, oldLevel.position.y, oldLevel.position.z-20), false));
                StartCoroutine(MoveObj.MoveAtoB(newLevel, new Vector3(newLevel.position.x, newLevel.position.y, newLevel.position.z-20), true));
            }
        }
    }


    private void TriggerNewLevel()
    {
        if (!is_open)
        {
             Debug.Log("OPEN NEW LEVEL");
             newLevel.gameObject.SetActive(true);
             gameObject.GetComponent<MeshRenderer>().material = open_trigger;

             StartCoroutine(MoveObj.MoveAtoB(newLevel, new Vector3(init_Pos.x, oldLevel.position.y, init_Pos.z), true)); 
        }
        else
        {
             Debug.Log("CLOSE NEW LEVEL");
             gameObject.GetComponent<MeshRenderer>().material = close_trigger;

             StartCoroutine(MoveObj.MoveAtoB(newLevel, init_Pos, false));
        }
        is_open = !is_open;
    }

    private void OnCollisionExit(Collision collision)
    {
        is_open = !is_open;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public float maxHeight = 3.5f, minHeight = 1.5f;
    private void Start()
    {
        GameEvent.instance.onDoorTrigger_Enter += OnDoorOpen;
        GameEvent.instance.onDoorTrigger_Exit += OnDoorClose;
    }

    private void OnDestroy()
    {
        GameEvent.instance.onDoorTrigger_Enter -= OnDoorOpen;
        GameEvent.instance.onDoorTrigger_Exit -= OnDoorClose;
    }


    private void OnDoorOpen()
    {
        StopAllCoroutines();
        StartCoroutine(MoveObj.MoveAtoB(transform, new Vector3(transform.position.x, maxHeight, transform.position.z), true));
    }

    private void OnDoorClose()
    {
        StopAllCoroutines();
        StartCoroutine(MoveObj.MoveAtoB(transform, new Vector3(transform.position.x, minHeight, transform.position.z), true));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class Obj
{
    public float angle =0;
    public float length = 4;
}

public class Pendulum : MonoBehaviour
{
    public float angle;
    public float length;
    public float shrinkSpeed;
    public float changePos;
    public float angleSpeed;
    public bool test = false;
    public bool change = false;

    // obj[i].position = new Vector3(x, y, 0);
    public Transform[] obj;

    void Update()
    {
        if (test)
        {
            angle += angleSpeed * Time.deltaTime;
            if (length < 0) change = false;
            if (length > 5) change = true;

            if (change)
               length -= shrinkSpeed * Time.deltaTime;
            else length += shrinkSpeed * Time.deltaTime;
        }

        for(int i=0; i<obj.Length; i++)
        {
            float x = transform.position.x + length * Mathf.Sin(angle + i * changePos);
            float y = transform.position.y - length * Mathf.Cos(angle + i * changePos);
            obj[i].position = new Vector3(x, y, 0);
        }



    }
}

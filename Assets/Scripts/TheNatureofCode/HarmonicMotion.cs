﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarmonicMotion : MonoBehaviour
{
    public float range = 5f;
    public float speed = 10f;


    void Update()
    {
        if(gameObject.name.Equals("Player"))
            transform.position = new Vector3(0, Mathf.Sin(Time.time * speed) * range, 0);
        if (gameObject.name.Equals("GameObject"))
            transform.localScale = new Vector3(Mathf.Sin(Time.time * speed) * range, 1, 1);
    }
}

﻿using System;

[Serializable]
public class Object
{
    public Vector2D position;
    public Vector2D velocity;
    public Vector2D acceleration;
    public Object()
    {
        position = new Vector2D();
        velocity = new Vector2D();
        acceleration = new Vector2D();
    }

    public void ApplyForce(Vector2D force)
    {
        acceleration += force;
    }
}

﻿using System;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Object mainPlayer = new Object();

    private Camera mainCam;
    public float minX, maxX, minY, maxY;

    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    void Start()
    {
        mainCam = Camera.main;

        mainPlayer.position.x = transform.position.x;
        mainPlayer.position.y = transform.position.y;
    }

    private void FixedUpdate()
    {
        
        //Command();

    }

    private void Command()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = mainCam.ScreenToWorldPoint(Input.mousePosition + new Vector3(0,0, -mainCam.transform.position.z));
            FollowTheMouse(mousePos);

        }
       
    }

    private void FollowTheMouse(Vector3 mousePos)
    {
        Vector2D mousePosQ = new Vector2D(mousePos.x, mousePos.y);
        Debug.Log("NEW METHOD : " + mousePosQ);
        Vector2D distance = mousePosQ - mainPlayer.position;

        distance.SetMagnitude(5);

        mainPlayer.acceleration = distance;
    }


    private void Update()
    {
        Vector2D gravity = new Vector2D(0,-10f);
        Vector2D wind = new Vector2D(10,0);
        mainPlayer.ApplyForce(gravity);

        if(Input.GetMouseButton(0))
        mainPlayer.ApplyForce(wind);

        AddAcceleration();
        CheckEdge();
        AddSpeed();
    }


    private void LateUpdate()
    {
        PositionPass();

        mainPlayer.acceleration = Vector2D.zero;
    }


    private void AddAcceleration()
    {
        mainPlayer.velocity += mainPlayer.acceleration * Time.deltaTime;

        //constraint
        float min = 100;
        if (mainPlayer.velocity.x > min) mainPlayer.velocity.x = min;
        if (mainPlayer.velocity.y > min) mainPlayer.velocity.y = min;
        if (mainPlayer.velocity.x < -min) mainPlayer.velocity.x = -min;
        if (mainPlayer.velocity.y < -min) mainPlayer.velocity.y = -min;
    }


    private void PositionPass()
    {
        transform.position = new Vector3(mainPlayer.position.x, mainPlayer.position.y, transform.position.z);
    }

    private void AddSpeed()
    {
        mainPlayer.position += mainPlayer.velocity * Time.deltaTime;
    }

    private void CheckEdge()
    {
        if (mainPlayer.position.x > maxX) mainPlayer.velocity.x = -Math.Abs(mainPlayer.velocity.x);
        if (mainPlayer.position.x < minX) mainPlayer.velocity.x = Math.Abs(mainPlayer.velocity.x);
        if (mainPlayer.position.y > maxY) mainPlayer.velocity.y = -Math.Abs(mainPlayer.velocity.y);
        if (mainPlayer.position.y < minY) mainPlayer.velocity.y = Math.Abs(mainPlayer.velocity.y);
    }
   
}

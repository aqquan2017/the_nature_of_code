﻿using System;
using System.Runtime.CompilerServices;

[Serializable]
public class Vector2D
{
    public float x, y;
    public readonly float z = 0;
    public Vector2D()
    {
        x = 0;
        y = 0;
    }
    public Vector2D(float x, float y)
    {
        this.x = x;
        this.y = y;
    }
    public static Vector2D down = new Vector2D(0, -1);
    public static Vector2D up = new Vector2D(0, 1);
    public static Vector2D left = new Vector2D(-1, 0);
    public static Vector2D right = new Vector2D(1, 0);
    public static Vector2D zero = new Vector2D(0, 0);
    public float Magnitude => (float)Math.Sqrt( x * x + y * y);
    public void Normalize()
    {
        float mag = Magnitude;
        x /= mag;
        y /= mag;
    }

    public void SetMagnitude(float var)
    {
        Normalize();
        x *= var;
        y *= var;
    }


    public static Vector2D operator +(Vector2D a, Vector2D b) => new Vector2D(a.x + b.x, a.y + b.y);
    public static Vector2D operator -(Vector2D a, Vector2D b) => new Vector2D(a.x - b.x, a.y - b.y);
    public static Vector2D operator *(Vector2D a, float n) => new Vector2D(a.x * n , a.y * n);
    //public static bool operator ==(Vector2D a, Vector2D b) => a.x == b.x && a.y == b.y;
    //public static bool operator !=(Vector2D a, Vector2D b) => a.x != b.x || a.y != b.y;

}
